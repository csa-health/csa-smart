import { ACTIONS } from '../constants/action-type';
const INITIAL_STATE = {
  users: {},
  awatingApproval: {},
  creditBalanceMembers: {},
  creditTransactions: {},
  locationWiseMember: {},
  registerMembers: {},
  unDelevered: {}
};

const applySetUsers = (state, action) => ({
  ...state,
  users: action.users
});

function userReducer(state = INITIAL_STATE, action) {
  switch(action.type) {
    case ACTIONS.USERS_SET : {
      return Object.assign({ ...state, users: action.users });
    }
    case ACTIONS.AWATING_APPROVAL : {
      return Object.assign({ ...state, awatingApproval: action.awatingApproval });
    }
    case ACTIONS.CREDIT_BALANCE_MEMBERS : {
      return Object.assign({ ...state, creditBalanceMembers: action.creditBalanceMembers });
    }
    default : return state;
  }
}

export default userReducer;