import { combineReducers } from 'redux';
import sessionReducer from './session';
import userReducer from './user';
import contactsListReducer from './contacts';

const rootReducer = combineReducers({
  sessionState: sessionReducer,
  userState: userReducer,
  contactState: contactsListReducer
});

export default rootReducer;
