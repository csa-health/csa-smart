const INITIAL_STATE = {
    contacts: {},
};

const applySetUsers = (state, action) => ({
    ...state,
    contacts: action.contacts || {}
});

const removeItem = (state, value) => ({
    ...state, 
    contacts: Object.keys(state.contacts).reduce((result, key) => {
                if (key !== value) {
                    result[key] = state.contacts[key];
                }
                    return result;
                }, {})
})

function contactsListReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'LIST_OF_CONTACTS': {
            return applySetUsers(state, action);
        }
        case 'REMOVE_ITEM': {
            return removeItem(state, action.contacts.id)
        }
        default:
            return state;
    }
}



export default contactsListReducer;