import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import { SignUpLink } from '../SignUp';
import { PasswordForgetForm } from '../PasswordForget';
import { auth } from '../../firebase';
import * as routes from '../../constants/routes';

const SignInPage = ({ history }) => (
  <section className="login-register">
      <SignInForm history={history} />
  </section>
)

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
  isVisible: false
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
    this.handleForgot = this.handleForgot.bind(this);
  }

  onSubmit = (event) => {
    const { email, password, } = this.state;
    const { history } = this.props;
    
    auth.doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        history.push(routes.HOME);
      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });
    event.preventDefault();
  }

  handleForgot() {
      this.setState(updateByPropertyName('isVisible',true));
  }

  render() {
    const { email, password, error, } = this.state;
    const isInvalid = password === '' || email === '';

    return (
          <div className="login-box card">
              <div className="card-body">
                  <form onSubmit={this.onSubmit} className="form-horizontal form-material">
                      <div className="form-group m-t-40">
                          <div className="col-xs-12">
                              <input className="form-control" 
                                value={email}
                                onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
                                type="text"
                                placeholder="Username" />
                          </div>
                      </div>
                      <div className="form-group">
                          <div className="col-xs-12">
                              <input className="form-control" 
                                value={password} 
                                onChange={event => this.setState(updateByPropertyName('password', event.target.value))} 
                                type="password"
                                placeholder="Password" />
                          </div>
                      </div>
                      <div>
                        { error && <p className="text-danger">{error.message}</p> }
                      </div>
                      <div className="form-group row">
                          <div className="col-md-12">
                              <div className="custom-control custom-checkbox">
                                  <input type="checkbox" className="custom-control-input" id="rememberMe" />
                                  <label className="custom-control-label" htmlFor="rememberMe">Remember me</label>
                                  <span role="button" className="text-dark pull-right" onClick={this.handleForgot}>
                                    <i className="fa fa-lock m-r-5"></i>
                                    Forgot Password?
                                  </span> 
                              </div>
                          </div>
                      </div>
                      <div className="form-group text-center m-t-20">
                          <div className="col-xs-12">
                              <button disabled={isInvalid}  className="btn btn-info btn-md btn-block text-uppercase" type="submit">Log In</button>
                          </div>
                      </div>
                      <div className="row">
                          <div className="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                              <div className="social"><a href="javascript:void(0)" className="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" className="fa fa-facebook"></i> </a> <a href="javascript:void(0)" className="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" className="fa fa-google-plus"></i> </a> </div>
                          </div>
                      </div>
                      <div className="form-group m-b-0">
                          <div className="col-sm-12 text-center">
                              <SignUpLink />
                          </div>
                      </div>
                  </form>

                <div className={this.state.isVisible ? 'animated slideInUp':'d-none'}>
                    <div className="">
                        <PasswordForgetForm />
                    </div>
                </div>
                  
              </div>

          </div>
    );
  }
}

export default withRouter(SignInPage);

export {
  SignInForm,
};
