import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';

import { auth } from '../../firebase';
import * as routes from '../../constants/routes';

import {  SignUpLink, SignInLink } from '../SignUp/'

const PasswordForgetPage = () =>
  <div className="container">
    <div className="d-flex justify-content-center flex-column  col-4">
      <h1 className="text-center mb-3">Password Forget</h1>
      <PasswordForgetForm />
      <SignUpLink />
      <SignInLink />
    </div>
  </div>

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  email: '',
  error: null,
};

class PasswordForgetForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const { email } = this.state;

    auth.doPasswordReset(email)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });

    event.preventDefault();
  }

  render() {
    const {
      email,
      error,
    } = this.state;

    const isInvalid = email === '';

    return (
      <div className="card mb-3 border-0">
        <div className="card-body p-0">
          <form onSubmit={this.onSubmit} className="d-flex flex-column flex-fill">
            <input
              value={this.state.email}
              onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
              type="text"
              placeholder="Email Address"
              className="form-control mb-3"
            />
            <button disabled={isInvalid} type="submit" className="btn btn-info btn-md btn-block"> Reset Password </button>

            { error && <p>{error.message}</p> }
          </form>
        </div>
      </div>
    );
  }
}

const PasswordForgetLink = () => <NavLink exact activeClassName="text-muted" to={routes.PASSWORD_FORGET}>Forgot Password?</NavLink>;

export default PasswordForgetPage;

export {
  PasswordForgetForm,
  PasswordForgetLink,
};
