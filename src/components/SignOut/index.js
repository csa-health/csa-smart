import React from 'react';

import { auth } from '../../firebase';

const SignOutButton = () => (
  <span className="btn btn-link text-primary" onClick={auth.doSignOut}>
    <em className="fa fa-power-off" />
    <span className="ml-1"></span>
  </span>
);

export default SignOutButton;
