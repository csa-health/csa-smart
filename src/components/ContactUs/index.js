import React, { Component } from 'react';
import { updateByPropertyName, guid } from '../../common/utiliti-helper';
import { withRouter } from 'react-router-dom';
import withAuthorization from '../Session/withAuthorization';
import { contactUsDB } from '../../firebase';
import * as routes from '../../constants/routes';
import { connect } from 'react-redux';
import { compose } from 'recompose';

const INITIAL_STATE = {
    fname: '', 
    lname: '', 
    email: '',
    subject: '',
    message: '', 
    error: null,
  };

const ContactList = ({contact, editItem, removeItem}) => 
    <li className="list-group-item d-flex justify-content-between align-items-center px-2 p-0"> 
        <div>
            { contact.fname }
            { contact.lname }
            { contact.email }
            { contact.message }
            { contact.subject }
        </div>
        <span className="badge">
            <EditButton editItem={editItem.bind(this, contact)} />
            <DeleteButton removeItem={removeItem.bind(this, contact)} />
        </span>
    </li>;

const DeleteButton = ({removeItem}) => (
    <button type="button" className="btn p-0" onClick={removeItem}>
        <em className="fa fa-trash text-danger" />
    </button>
);
    
const EditButton = ({editItem}) => (
    <button type="button" className="btn px-2 p-0" onClick={editItem}>
        <em className="fa fa-edit text-success" />
</button>);

class ContactUs extends Component{

    constructor(props) {
        super(props);
        this.state = { ...INITIAL_STATE };
        this.onSubmit = this.onSubmit.bind(this);
        this.removeEvent = this.removeEvent.bind(this);
        this.editItemEvent = this.editItemEvent.bind(this);
    }

    componentDidMount() {
        const { onSetContacts } = this.props;
        
        contactUsDB.onceGetContact().then((response) => {
            onSetContacts(response.data.contactList)
            // snapshot.forEach(function(childSnapshot) {
            //     var childKey = childSnapshot.key;
            //     var childData = childSnapshot.val();
            //     console.log(childKey,'=======',childData);
            // });
        })
        .catch((error, data) => {
            console.log( error,' data ', data );
        });
    }

    onSubmit(event) {
        const id = guid();
        const { fname, lname, email, subject, message } = this.state;
        const { history } = this.props;
        
        contactUsDB.doCreateContact(id, fname, lname, email, subject, message).then((res) => {
            this.setState(() => ({ ...INITIAL_STATE }));
            history.push(routes.HOME);
          })
          .catch(error => {
            this.setState(updateByPropertyName('error', error));
          });
        event.preventDefault();
    }

    render() {
        const { fname, lname, email, subject, message } = this.state;
        const { contactsList=[] } = this.props;
        console.log('Contact List: ', contactsList, '====', !contactsList.length );
        return ( 
            <div className='container'>
                <div className='row'>
                    <div className='col-sm-10 col-sm-offset-1'>
                        <div className='well'>
                            <form>
                                <div className='row'>
                                    <div className='col-sm-4'>
                                        <div className='form-group'>
                                            <label htmlFor='fname'>First Name</label>
                                            <input type='text' name='fname' value={fname} className='form-control'
                                            onChange={event => this.setState(updateByPropertyName('fname', event.target.value))} />
                                        </div>
                                        <div className='form-group'>
                                            <label htmlFor='lname'>Last Name</label>
                                            <input type='text' name='lname' value={lname} className='form-control' 
                                            onChange={event => this.setState(updateByPropertyName('lname', event.target.value))} />
                                        </div>
                                        <div className='form-group'>
                                            <label htmlFor='email'>Email</label>
                                            <input type='text' name='email' value={email} className='form-control' 
                                            onChange={event => this.setState(updateByPropertyName('email', event.target.value))}/>
                                        </div>
                                        <div className='form-group'>
                                            <label htmlFor='subject'>Subject</label>
                                            <select name='subject' value={subject} className='form-control'
                                            onChange={event => this.setState(updateByPropertyName('subject', event.target.value))}>
                                                <option>General Inquiry</option>
                                                <option>Site Suggestions</option>
                                                <option>Product Support</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className='col-sm-8'>
                                        <div className='form-group'>
                                            <label htmlFor='message'>Message</label>
                                            <textarea className='form-control' name="message" value={message} rows='10'
                                            onChange={event => this.setState(updateByPropertyName('message', event.target.value))}></textarea>
                                        </div>
                                        <div className='text-right'>
                                            <input type='button' onClick={this.onSubmit} className='btn btn-primary' value='Submit' />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div> 
                    <ul className="list-group">
                    {
                        (!contactsList.length) ? 'no records' :
                        contactsList.map((item, key) => <ContactList key={key} 
                            contact={item}
                            editItem={this.editItemEvent} 
                            removeItem={this.removeEvent}  />)
                    }
                    </ul>
                </div>
            </div>
         );
    }

    removeEvent(item) {
        const { onRemoveChild } = this.props;
        contactUsDB.doRemoveContact(item.id).then(snapshot => {
            onRemoveChild(item);
        })
    }

    editItemEvent(item) {
        this.setState(() => item);
    }
}


const mapStateToProps = (state) => ({
    contactsList: state.contactState.contacts,
});

const mapDispatchToProps = (dispatch) => ({
    onSetContacts: (contacts) => dispatch({ type: 'LIST_OF_CONTACTS', contacts}),
    onRemoveChild: (contacts) => dispatch({ type: 'REMOVE_ITEM', contacts})
})

const authCondition = (authUser) => !!authUser;
export default compose(
    withAuthorization(authCondition),
    connect(mapStateToProps, mapDispatchToProps)
)( withRouter(ContactUs) );