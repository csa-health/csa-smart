import Navigation from '../Navigation';

const MasterLayout = ({children}) => (
    <div id="main-wrapper">
      <Navigation />

      {/* Left Side Bar */}
      <aside className="left-sidebar">
        <div className="d-flex no-block nav-text-box align-items-center">
            <span>Bank of Philliphines</span>
            <a className="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i className="mdi mdi-toggle-switch"></i></a>
            <a className="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i className="ti-menu ti-close"></i></a>
        </div>
        <div className="scroll-sidebar ps ps--theme_default ps--active-y" data-ps-id="5736a899-f0af-9169-e1eb-87eccaf321a6">
                
                <nav className="sidebar-nav">
                    <ul id="sidebarnav">
                        <li className="selected"> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="icon-speedometer"></i><span className="hide-menu">Dashboard <span className="badge badge-pill badge-cyan">4</span></span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="index.html">Minimal <i className="fa fa-circle-o text-success"></i></a></li>
                                <li className="active"><a href="index2.html" className="active">Analytical <i className="fa fa-circle-o text-info"></i></a></li>
                                <li><a href="index3.html">Demographical <i className="fa fa-circle-o text-danger"></i></a></li>
                                <li><a href="index4.html">Modern <i className="fa fa-circle-o text-warning"></i></a></li>
                                <li><a href="index5.html">Cryptocurrency <i className="fa fa-circle-o text-primary"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-layout-grid2"></i><span className="hide-menu">Apps</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="app-calendar.html">Calendar <i className="ti-calendar"></i></a></li>
                                <li><a href="app-chat.html">Chat app <i className="ti-comment"></i></a></li>
                                <li><a href="app-ticket.html">Support Ticket <i className="ti-support"></i></a></li>
                                <li><a href="app-contact.html">Contact / Employee <i className="ti-user"></i></a></li>
                                <li><a href="app-contact2.html">Contact Grid <i className=" ti-list"></i></a></li>
                                <li><a href="app-contact-detail.html">Contact Detail <i className="ti-pencil-alt"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-email"></i><span className="hide-menu">Inbox</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="app-email.html">Mailbox <i className="icon-envelope-open"></i></a></li>
                                <li><a href="app-email-detail.html">Mailbox Detail <i className="ti-layout-media-left-alt"></i></a></li>
                                <li><a href="app-compose.html">Compose Mail <i className="icon-note"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-palette"></i><span className="hide-menu">Ui Elements <span className="badge badge-pill badge-primary text-white">25</span></span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="ui-cards.html">Cards <i className="ti-layers-alt"></i></a></li>
                                <li><a href="ui-user-card.html">User Cards <i className="ti-user"></i></a></li>
                                <li><a href="ui-buttons.html">Buttons <i className="ti-layout-menu"></i></a></li>
                                <li><a href="ui-modals.html">Modals <i className="ti-layout-slider-alt"></i></a></li>
                                <li><a href="ui-tab.html">Tab <i className="ti-layout-tab-min"></i></a></li>
                                <li><a href="ui-tooltip-popover.html">Tooltip &amp; Popover <i className="ti-pin2"></i></a></li>
                                <li><a href="ui-tooltip-stylish.html">Tooltip stylish <i className="ti-themify-favicon"></i></a></li>
                                <li><a href="ui-sweetalert.html">Sweet Alert <i className="ti-comments-smiley"></i></a></li>
                                <li><a href="ui-notification.html">Notification <i className="ti-alert"></i></a></li>
                                <li><a href="ui-progressbar.html">Progressbar <i className="ti-layout-list-post"></i></a></li>
                                <li><a href="ui-nestable.html">Nestable <i className="ti-layout-accordion-separated"></i></a></li>
                                <li><a href="ui-range-slider.html">Range slider <i className="ti-layout-slider-alt"></i></a></li>
                                <li><a href="ui-timeline.html">Timeline <i className="ti-dashboard"></i></a></li>
                                <li><a href="ui-typography.html">Typography <i className="ti-more-alt"></i></a></li>
                                <li><a href="ui-horizontal-timeline.html">Horizontal Timeline <i className="ti-layout-list-thumb"></i></a></li>
                                <li><a href="ui-session-timeout.html">Session Timeout <i className="ti-na"></i></a></li>
                                <li><a href="ui-session-ideal-timeout.html">Session Ideal Timeout <i className="ti-time"></i></a></li>
                                <li><a href="ui-bootstrap.html">Bootstrap Ui <i className="ti-rocket"></i></a></li>
                                <li><a href="ui-breadcrumb.html">Breadcrumb <i className="icon-directions"></i></a></li>
                                <li><a href="ui-bootstrap-switch.html">Bootstrap Switch <i className="ti-flickr"></i></a></li>
                                <li><a href="ui-list-media.html">List Media <i className="icon-grid"></i></a></li>
                                <li><a href="ui-ribbons.html">Ribbons <i className="ti-medall"></i></a></li>
                                <li><a href="ui-grid.html">Grid <i className="ti-layout-grid3-alt"></i></a></li>
                                <li><a href="ui-carousel.html">Carousel <i className="ti-layout-slider"></i></a></li>
                                <li><a href="ui-date-paginator.html">Date-paginator <i className="icon-calender"></i></a></li>
                                <li><a href="ui-dragable-portlet.html">Dragable Portlet <i className="ti-move"></i></a></li>
                            </ul>
                        </li>
                        <li className="nav-small-cap"></li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-layout-media-right-alt"></i><span className="hide-menu">Forms</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="form-basic.html">Basic Forms <i className="ti-file"></i></a></li>
                                <li><a href="form-layout.html">Form Layouts <i className="ti-files"></i></a></li>
                                <li><a href="form-addons.html">Form Addons <i className="ti-zip"></i></a></li>
                                <li><a href="form-material.html">Form Material <i className="ti-agenda"></i></a></li>
                                <li><a href="form-float-input.html">Floating Lable <i className="ti-receipt"></i></a></li>
                                <li><a href="form-pickers.html">Form Pickers <i className="ti-write"></i></a></li>
                                <li><a href="form-upload.html">File Upload <i className="ti-upload"></i></a></li>
                                <li><a href="form-mask.html">Form Mask <i className="ti-layers"></i></a></li>
                                <li><a href="form-validation.html">Form Validation <i className="icon-info"></i></a></li>
                                <li><a href="form-dropzone.html">File Dropzone <i className="ti-layout-sidebar-2"></i></a></li>
                                <li><a href="form-icheck.html">Icheck control <i className="icon-eye"></i></a></li>
                                <li><a href="form-img-cropper.html">Image Cropper <i className="icon-crop"></i></a></li>
                                <li><a href="form-bootstrapwysihtml5.html">HTML5 Editor <i className="ti-html5"></i></a></li>
                                <li><a href="form-typehead.html">Form Typehead <i className="ti-text"></i></a></li>
                                <li><a href="form-wizard.html">Form Wizard <i className="ti-new-window"></i></a></li>
                                <li><a href="form-xeditable.html">Xeditable Editor <i className="ti-palette"></i></a></li>
                                <li><a href="form-summernote.html">Summernote Editor <i className="ti-pin2"></i></a></li>
                                <li><a href="form-tinymce.html">Tinymce Editor <i className="ti-text"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-layout-accordion-merged"></i><span className="hide-menu">Tables</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="table-basic.html">Basic Tables <i className="ti-widgetized"></i></a></li>
                                <li><a href="table-layout.html">Table Layouts <i className="ti-layout-accordion-list"></i></a></li>
                                <li><a href="table-data-table.html">Data Tables <i className="ti-widget"></i></a></li>
                                <li><a href="table-footable.html">Footable <i className="ti-layout-accordion-merged"></i></a></li>
                                <li><a href="table-jsgrid.html">Js Grid Table <i className="ti-layout-placeholder"></i></a></li>
                                <li><a href="table-responsive.html">Responsive Table <i className="ti-layout-accordion-separated"></i></a></li>
                                <li><a href="table-bootstrap.html">Bootstrap Tables <i className="ti-layout"></i></a></li>
                                <li><a href="table-editable-table.html">Editable Table <i className="ti-pencil-alt"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-settings"></i><span className="hide-menu">Widgets</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="widget-data.html">Data Widgets <i className="icon-puzzle"></i></a></li>
                                <li><a href="widget-apps.html">Apps Widgets <i className="icon-drawar"></i></a></li>
                                <li><a href="widget-charts.html">Charts Widgets <i className="ti-bar-chart-alt"></i></a></li>
                            </ul>
                        </li>
                        <li className="nav-small-cap"></li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-gallery"></i><span className="hide-menu">Page Layout</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="layout-single-column.html">1 Column <i className="ti-layout-menu-separated"></i></a></li>
                                <li><a href="layout-fix-header.html">Fix header <i className="mdi mdi-page-layout-header"></i></a></li>
                                <li><a href="layout-fix-sidebar.html">Fix sidebar <i className="mdi mdi-page-layout-sidebar-left"></i></a></li>
                                <li><a href="layout-fix-header-sidebar.html">Fixe header &amp; Sidebar <i className="ti-agenda"></i></a></li>
                                <li><a href="layout-boxed.html">Boxed Layout <i className="mdi mdi-page-layout-body"></i></a></li>
                                <li><a href="layout-logo-center.html">Logo in Center <i className="icon-size-actual"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-files"></i><span className="hide-menu">Sample Pages <span className="badge badge-pill badge-info">25</span></span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="starter-kit.html">Starter Kit <i className="ti-layout-width-default"></i></a></li>
                                <li><a href="pages-blank.html">Blank page <i className="mdi mdi-content-copy"></i></a></li>
                                <li><a href="javascript:void(0)" className="has-arrow">Authentication <span className="badge badge-pill badge-success pull-right">6</span></a>
                                    <ul aria-expanded="false" className="collapse">
                                        <li><a href="pages-login.html">Login 1 <i className="ti-lock"></i></a></li>
                                        <li><a href="pages-login-2.html">Login 2 <i className="icon-user-following"></i></a></li>
                                        <li><a href="pages-register.html">Register <i className="ti-marker-alt"></i></a></li>
                                        <li><a href="pages-register2.html">Register 2 <i className="ti-marker"></i></a></li>
                                        <li><a href="pages-register3.html">Register 3 <i className="ti-marker-alt"></i></a></li>
                                        <li><a href="pages-lockscreen.html">Lockscreen <i className="icon-lock"></i></a></li>
                                        <li><a href="pages-recover-password.html">Recover password <i className="icon-lock-open"></i></a></li>
                                    </ul>
                                </li>
                                <li><a href="pages-profile.html">Profile page <i className="ti-id-badge"></i></a></li>
                                <li><a href="pages-animation.html">Animation <i className="ti-video-clapper"></i></a></li>
                                <li><a href="pages-fix-innersidebar.html">Sticky Left sidebar <i className="ti-layout-sidebar-left"></i></a></li>
                                <li><a href="pages-fix-inner-right-sidebar.html">Sticky Right sidebar <i className="ti-layout-sidebar-right"></i></a></li>
                                <li><a href="pages-invoice.html">Invoice <i className="icon-speech"></i></a></li>
                                <li><a href="pages-treeview.html">Treeview <i className="ti-layout-menu-v"></i></a></li>
                                <li><a href="pages-utility-classes.html">Helper Classes <i className="icon-compass"></i></a></li>
                                <li><a href="pages-search-result.html">Search result <i className="icon-hourglass"></i></a></li>
                                <li><a href="pages-scroll.html">Scrollbar <i className="icon-equalizer"></i></a></li>
                                <li><a href="pages-pricing.html">Pricing <i className="ti-money"></i></a></li>
                                <li><a href="pages-lightbox-popup.html">Lighbox popup <i className="ti-light-bulb"></i></a></li>
                                <li><a href="pages-gallery.html">Gallery <i className="ti-gallery"></i></a></li>
                                <li><a href="pages-faq.html">Faqs <i className="icon-social-foursqare"></i></a></li>
                                <li><a href="javascript:void(0)" className="has-arrow">Error Pages</a>
                                    <ul aria-expanded="false" className="collapse">
                                        <li><a href="pages-error-400.html">400 <i className="ti-info-alt"></i></a></li>
                                        <li><a href="pages-error-403.html">403 <i className="ti-info"></i></a></li>
                                        <li><a href="pages-error-404.html">404 <i className="ti-face-sad"></i></a></li>
                                        <li><a href="pages-error-500.html">500 <i className="ti-help"></i></a></li>
                                        <li><a href="pages-error-503.html">503 <i className="ti-infinite"></i></a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-pie-chart"></i><span className="hide-menu">Charts</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="chart-morris.html">Morris Chart <i className="ti-stats-up"></i></a></li>
                                <li><a href="chart-chartist.html">Chartis Chart <i className="ti-bar-chart"></i></a></li>
                                <li><a href="chart-echart.html">Echarts <i className="icon-chart"></i></a></li>
                                <li><a href="chart-flot.html">Flot Chart <i className="mdi-chart-histogram"></i></a></li>
                                <li><a href="chart-knob.html">Knob Chart <i className="ti-pulse"></i></a></li>
                                <li><a href="chart-chart-js.html">Chartjs <i className="mdi mdi-chart-areaspline"></i></a></li>
                                <li><a href="chart-sparkline.html">Sparkline Chart <i className="icon-graph"></i></a></li>
                                <li><a href="chart-extra-chart.html">Extra chart <i className="ti-stats-down"></i></a></li>
                                <li><a href="chart-peity.html">Peity Charts <i className="mdi mdi-chart-pie"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-light-bulb"></i><span className="hide-menu">Icons</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="icon-material.html">Material Icons <i className="icon-social-reddit"></i></a></li>
                                <li><a href="icon-fontawesome.html">Fontawesome Icons <i className="ti-github"></i></a></li>
                                <li><a href="icon-themify.html">Themify Icons <i className="ti-themify-logo"></i></a></li>
                                <li><a href="icon-weather.html">Weather Icons <i className="ti-shine"></i></a></li>
                                <li><a href="icon-simple-lineicon.html">Simple Line icons <i className="icon-emotsmile"></i></a></li>
                                <li><a href="icon-flag.html">Flag Icons <i className="ti-flag"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-location-pin"></i><span className="hide-menu">Maps</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="map-google.html">Google Maps <i className="mdi mdi-google-maps"></i></a></li>
                                <li><a href="map-vector.html">Vector Maps <i className="mdi mdi-map-marker"></i></a></li>
                            </ul>
                        </li>
                        <li> <a className="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="ti-align-left"></i><span className="hide-menu">Multi level dd</span></a>
                            <ul aria-expanded="false" className="collapse">
                                <li><a href="javascript:void(0)">item 1.1<i className="ti-align-left"></i></a></li>
                                <li><a href="javascript:void(0)">item 1.2<i className="ti-align-left"></i></a></li>
                                <li> <a className="has-arrow" href="javascript:void(0)" aria-expanded="false">Menu 1.3<i className="ti-align-left"></i></a>
                                    <ul aria-expanded="false" className="collapse">
                                        <li><a href="javascript:void(0)">item 1.3.1<i className="ti-list-ol"></i></a></li>
                                        <li><a href="javascript:void(0)">item 1.3.2<i className="ti-list-ol"></i></a></li>
                                        <li><a href="javascript:void(0)">item 1.3.3<i className="ti-list-ol"></i></a></li>
                                        <li><a href="javascript:void(0)">item 1.3.4<i className="ti-list-ol"></i></a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">item 1.4<i className="ti-align-left"></i></a></li>
                            </ul>
                        </li>
                        <li className="nav-small-cap"></li>
                        <li> <a className="waves-effect waves-dark" href="../documentation/documentation.html" aria-expanded="false"><i className="fa fa-circle-o text-danger"></i><span className="hide-menu">Documentation</span></a></li>
                        <li> <a className="waves-effect waves-dark" href="pages-login.html" aria-expanded="false"><i className="fa fa-circle-o text-success"></i><span className="hide-menu">Log Out</span></a></li>
                        <li> <a className="waves-effect waves-dark" href="pages-faq.html" aria-expanded="false"><i className="fa fa-circle-o text-info"></i><span className="hide-menu">FAQs</span></a></li>
                    </ul>
                </nav>
            <div className="ps__scrollbar-x-rail"><div className="ps__scrollbar-x"></div></div><div className="ps__scrollbar-y-rail"><div className="ps__scrollbar-y"></div></div></div>
      </aside>

      {/* Main Page Container */}
      <div className="page-wrapper">
        <div className="container-fluid">
          { children }
        </div>
      </div>
    </div>
  );

export {
    MasterLayout
};