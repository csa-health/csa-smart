import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
// import Loadable from 'react-loadable';

// import { Loading } from '../../common/loading-spiner';
import { MasterLayout } from './master-layout';
import HomePage from '../Home';
import ContactUs from '../ContactUs';
import SignUpPage from '../SignUp';
import SignInPage from '../SignIn';
import PasswordForgetPage from '../PasswordForget';

import withAuthentication from '../Session/withAuthentication';
import * as routes from '../../constants/routes';

const App = () =>
  <Router>
    <div className="skin-default-dark fixed-layout">
        <Switch>
          <Route exact path={routes.SIGN_IN} component={() => <SignInPage />} />
          <Route exact path={routes.SIGN_UP} component={() => <SignUpPage />} />
          <Route exact path={routes.PASSWORD_FORGET} component={() => <PasswordForgetPage />} />
          <MasterLayout>
            <Switch>
              <Route exact path={routes.HOME} component={() => <HomePage />} />
              <Route exact path={routes.CONTACT_US} component={() => <ContactUs />} />
            </Switch>
          </MasterLayout>
        </Switch>
    </div>
  </Router>

export default withAuthentication(App);