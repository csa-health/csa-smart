import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import withAuthorization from '../Session/withAuthorization';
import { db } from '../../firebase';

import { LoaderHOC } from '../../common/loading-spiner';
import { ACTIONS } from '../../constants/action-type';

class HomePage extends Component {
  componentDidMount() {
    const { onSetUsers, 
      onSetAwaitingApproval, 
      onSetCreditBalanceMembers,
      onSetCreditTransactions,
      onSetLocationWiseMember,
      onSetRegisterMembers,
      onSetUnDelevered } = this.props;

    db.onceGetUsers().then( snapshot => onSetUsers(snapshot.val()) );
    db.getAwatingApproval().then( snapshot =>{
      onSetAwaitingApproval(snapshot.val()) 
    }).catch(error => console.log(error));
    db.getCreditBalanceMembers().then( snapshot => onSetCreditBalanceMembers(snapshot.val()) );
    db.getCreditTransactions().then( snapshot => onSetCreditTransactions(snapshot.val()) );
    db.getLocationWiseMember().then( snapshot => onSetLocationWiseMember(snapshot.val()) );
    db.getRegisteredMembers().then( snapshot => onSetRegisterMembers(snapshot.val()) );
    db.getUnDelevered().then( snapshot => onSetUnDelevered(snapshot.val()) );
  }

  handleRegisteredUser() {
    console.log('Registered User call');
  }

  render() {
    const { users } = this.props;

    return (
      <div className="">
        { !!users && <LoadingUserList users={users} registeredUser={this.handleRegisteredUser} /> }
      </div>
    );
  }
}

const UserList = ({ users, registeredUser }) =>
  <div>
    <h2 className="border-bottom pb-3">Dashboard</h2>
    <div className="d-flex justify-content-start flex-row flex-wrap">
      <ListGroup users={users} mainHeading="User" subHeading="Registered CSA Members" buttonLabel="Registered Members" handleList={registeredUser} />
      <ListGroup users={users} mainHeading="Awating Approval" subHeading="Pending Request" buttonLabel="Pending Request" handleList={registeredUser} />
      <ListGroup users={users} mainHeading="Undelivered" subHeading="Delivery Not Taken" buttonLabel="Undelivered List" handleList={registeredUser} />
      <ListGroup users={users} mainHeading="Members" subHeading="With Credit Balance < Rs. 1000" buttonLabel="Credit Balance" handleList={registeredUser} />
    </div>
  </div>

const ListGroup = ({users, mainHeading, subHeading, buttonLabel, handleList}) => (
  <div className="d-flex pr-4 mt-3">
    <div className="" style={{width: '20rem'}}>
      <h5 className="d-flex justify-content-start">
        <span className="">{ mainHeading }</span>
        <span className="ml-auto">{ Object.keys(users).length } </span>
      </h5>
      <h6 className="">{subHeading}</h6>
      <div className="">
        <ul className="list-group">
          {
            Object.keys(users).map(key =>
            <li key={key} className="list-group-item">{users[key].username}</li>
          )}
        </ul>
        <button className="btn btn-info btn-md btn-block" onClick={handleList}>{buttonLabel}</button>
      </div>
    </div>
    
  </div>
)

var LoadingUserList = LoaderHOC('users', 'registeredUser')(UserList);

const mapStateToProps = (state) => ({
  users: state.userState.users,
  awatingApproval: state.userState.awatingApproval,
  creditBalanceMembers: state.userState.creditBalanceMembers,
  creditTransactions: state.userState.creditTransactions,
  locationWiseMember: state.userState.locationWiseMember,
  registerMembers: state.userState.registerMembers,
  unDelevered: state.userState.unDelevered,
});

const mapDispatchToProps = (dispatch) => ({
  onSetUsers: (users) => dispatch({ type: ACTIONS.USERS_SET, users }),
  onSetAwaitingApproval: (awatingApproval) => dispatch({ type: ACTIONS.AWATING_APPROVAL, awatingApproval }),
  onSetCreditBalanceMembers: (creditBalanceMembers) => dispatch({ type: ACTIONS.CREDIT_BALANCE_MEMBERS, creditBalanceMembers }),
  onSetCreditTransactions: (creditTransactions) => dispatch({ type: ACTIONS.CREDIT_TRANSACTIONS, creditTransactions }),
  onSetLocationWiseMember: (locationWiseMember) => dispatch({ type: ACTIONS.LOCATION_WISE_MEMBER, locationWiseMember }),
  onSetRegisterMembers: (registerMembers) => dispatch({ type: ACTIONS.REGISTERED_MEMBERS, registerMembers }),
  onSetUnDelevered: (unDelevered) => dispatch({ type: ACTIONS.UN_DELEVERED, unDelevered }),
});

const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  connect(mapStateToProps, mapDispatchToProps)
)(HomePage);
