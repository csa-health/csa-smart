import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

import SignOutButton from '../SignOut';
import * as routes from '../../constants/routes';

const Navigation = ({ authUser }) => { return authUser ? <NavigationAuth user={authUser} /> : <NavigationNonAuth /> };

const NavigationAuth = ({user}) => (
  <header className="topbar">
  <nav className="navbar top-navbar navbar-expand-md navbar-dark">
  <div className="navbar-header">
    <ul className="nav">
      <li className="nav-item">
        <NavLink replace exact to={routes.HOME} className="nav-link" activeClassName="text-muted">
          <em className="ti-home" />
          <div className="notify"> <span className="heartbit"></span> <span className="point"></span> </div>
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink replace exact to={routes.CONTACT_US} className="nav-link" activeClassName="text-muted">
          <em className="icon-note" />
          <div className="notify"> <span className="heartbit"></span> <span className="point"></span> </div>
        </NavLink>
      </li>
    </ul>
    </div>
    <div className="ml-auto">
      <span>{user.email}</span> <SignOutButton />
    </div>
  </nav>
  </header>
);

const NavigationNonAuth = () => null;

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});

export default connect(mapStateToProps)(Navigation);
