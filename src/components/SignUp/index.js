import React, { Component } from 'react';
import {
  Link,
  withRouter,
} from 'react-router-dom';
import '../../assets/css/login.scss';

import { auth, db } from '../../firebase';
import * as routes from '../../constants/routes';

const SignUpPage = ({ history }) =>
  <section className="login-register">
    <div className="login-box card">
      <div className="card-body">
        <h1 className="text-center">Sign Up</h1>
        <img className="profile-img-card" src="../src/assets/images/avatar_2x.png" />
        <SignUpForm history={history} />
      </div>
    </div>
  </section>

const updateByPropertyName = (propertyName, value) => () => ({
  [propertyName]: value,
});

const INITIAL_STATE = {
  username: '',
  email: '',
  passwordOne: '',
  passwordTwo: '',
  error: null,
};

class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
  }

  onSubmit = (event) => {
    const {
      username,
      email,
      passwordOne,
    } = this.state;

    const {
      history,
    } = this.props;

    auth.doCreateUserWithEmailAndPassword(email, passwordOne)
      .then(authUser => {

        // Create a user in your own accessible Firebase Database too
        db.doCreateUser(authUser.user.uid, username, email)
          .then(() => {
            this.setState(() => ({ ...INITIAL_STATE }));
            history.push(routes.HOME);
          })
          .catch(error => {
            this.setState(updateByPropertyName('error', error));
          });

      })
      .catch(error => {
        this.setState(updateByPropertyName('error', error));
      });

    event.preventDefault();
  }

  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      error,
    } = this.state;

    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      username === '' ||
      email === '';

    return (
      <div className="form-group m-t-40">
        <form onSubmit={this.onSubmit} className="form-horizontal form-material">
          <div className="form-group m-t-40">
            <div className="col-xs-12">
              <input
                value={username}
                onChange={event => this.setState(updateByPropertyName('username', event.target.value))}
                type="text"
                placeholder="Full Name"
                className="form-control"
              />
              </div>
            </div>
          <div className="form-group m-t-40">
            <div className="col-xs-12">
              <input
                value={email}
                onChange={event => this.setState(updateByPropertyName('email', event.target.value))}
                type="text"
                placeholder="Email Address"
                className="form-control"
              />
            </div>
          </div>
          <div className="form-group m-t-40">
            <div className="col-xs-12">
              <input
                value={passwordOne}
                onChange={event => this.setState(updateByPropertyName('passwordOne', event.target.value))}
                type="password"
                placeholder="Password"
                className="form-control"
              />
              </div>
          </div>
          <div className="form-group m-t-40">
            <div className="col-xs-12">
              <input
                value={passwordTwo}
                onChange={event => this.setState(updateByPropertyName('passwordTwo', event.target.value))}
                type="password"
                placeholder="Confirm Password"
                className="form-control"
              />
            </div>
          </div>
          <button disabled={isInvalid} type="submit" className="btn btn-info btn-md btn-block btn-signin"> Sign Up </button>
          <SignInLink />

          { error && <p>{error.message}</p> }
        </form>
      </div>
    );
  }
}

const SignUpLink = () => (
  <p>
    Don't have an account?
    {' '}
    <Link to={routes.SIGN_UP}>Sign Up</Link>
  </p>
);

const SignInLink = () => (
  <p>
    <Link to={routes.SIGN_IN}>Sign In</Link>
  </p>
);

export default withRouter(SignUpPage);

export {
  SignUpForm,
  SignUpLink,
  SignInLink
};