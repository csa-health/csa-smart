import { updateObjectInArray, updateByPropertyName } from './utiliti-helper';

test('it should return array of objects', () => {
    const item = [{id:1, message:'this is first item'},{id:2, message:'this is second item'}]
    const action = { data: {id:4, updatedata:'this is first item'} };
    expect( updateObjectInArray(item, action) ).toEqual([ ...item, ...action.data ]);
});

test('it should return property of object', () => {
    const action = { message: "Error message displayed" };
    const newUpdateByPropertyName = updateByPropertyName('test', action);
    expect( newUpdateByPropertyName() ).toEqual({test: action});
});