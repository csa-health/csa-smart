import { db, functions } from './firebase';

// User API
export const doCreateUser = (id, username, email) =>
  db.ref(`users/${id}`).set({
    username,
    email,
  });

export const onceGetUsers = () => db.ref('users').once('value');

export const getAwatingApproval = () => db.ref('awatingApproval').once('value');

export const getCreditBalanceMembers = () => db.ref('creditBalanceMembers').once('value');

export const getCreditTransactions = () => db.ref('creditTransactions').once('value');

export const getLocationWiseMember = () => db.ref('locationWiseMember').once('value');

export const getRegisteredMembers = () => db.ref('registerMembers').once('value');

export const getUnDelevered = () => db.ref('unDelevered').once('value');


  // Other db APIs ...
