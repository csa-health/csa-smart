import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/functions';

const prodConfig = {
  apiKey: "AIzaSyAAOoW_l4sVEp4FDke6XSIG-VtZ0LbP4Q0",
  authDomain: "csa-smart.firebaseapp.com",
  databaseURL: "https://csa-smart.firebaseio.com",
  projectId: "csa-smart",
  storageBucket: "csa-smart.appspot.com",
  messagingSenderId: "325652646520"
};

const devConfig = {
  apiKey: "AIzaSyAAOoW_l4sVEp4FDke6XSIG-VtZ0LbP4Q0",
  authDomain: "csa-smart.firebaseapp.com",
  databaseURL: "https://csa-smart.firebaseio.com",
  projectId: "csa-smart",
  storageBucket: "csa-smart.appspot.com",
  messagingSenderId: "325652646520"
};

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();
const functions = firebase.functions();

export {
  db,
  auth,
  functions,
};
