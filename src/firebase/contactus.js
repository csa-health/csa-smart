import { db, functions } from './firebase';

//Contact us API
export const doCreateContact = (id, fname, lname, email, subject, message) => 
    db.ref(`contacts/${id}`).set({
        id,
        fname,
        lname,
        email,
        subject,
        message
    });
// console.log( functions.httpsCallable('contactList') );
// export const onceGetContact = () => db.ref('contacts').once('value');
export const onceGetContact = functions.httpsCallable('contactList');

export const doRemoveContact = (id) => db.ref(`contacts/${id}`).remove();