// import * as functions from 'firebase-functions';
// import * as admin from 'firebase-admin'
// import * as AddMessage from './add-message'
// import * as UpCaseMessages from './upcase-messages'

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
// Initialize the admin config, basically it is user session.
// admin.initializeApp(functions.config().firebase);

export * from "./main";
export * from "./http-functions";
export * from "./contacts";
export * from "./users";

