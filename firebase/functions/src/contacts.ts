import { db } from './db';
import { https } from 'firebase-functions';
import { filterObjects } from './utilityFunction';

const createContact = async (req: any, res: any) => {
    db.ref('contacts').push(req.body.contacts);
    res.send(req.body.contacts);
}
export const newContact = https.onRequest(createContact);

const GetContacts = async (req, res) => {
    const snapshot = await db.ref('contacts').once('value').then(snp => {
        return { contactList:  filterObjects(snp.val()) }
    });
    res.send({data: snapshot });
};
export const contactList = https.onRequest(GetContacts);
