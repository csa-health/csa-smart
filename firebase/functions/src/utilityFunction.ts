const filterObjects = (data: Object) => {
    return Object.keys(data).map(key => data[key]);
}

export {
    filterObjects
}