import { db } from './db';
import { https } from 'firebase-functions';
import { filterObjects } from './utilityFunction';

const createUser = async (req: any, res: any) => {
    db.ref('contacts').push(req.body.contacts);
    res.send(req.body.contacts);
}
export const newUser = https.onRequest(createUser);

const GetUsers = async (req, res) => {
    const snapshot = await db.ref('users').once('value').then(snp => { 
        return { userList:  filterObjects(snp.val()) } 
    });
    res.send( snapshot );
};
export const usersList = https.onRequest(GetUsers);
